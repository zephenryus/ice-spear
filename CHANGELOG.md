# Changelog

This changelog contains the changes for Ice-Spear and all sub modules. <br/>
The current released and prebuild version is: **1.0.0**

<hr/>

### Version 1.1.0 (WIP)
**Shrine-Editor** <br/>
#37 - fixed Actor dublication, byaml array-data is now correct (@byaml-lib)<br/>
#38 - PrOD files are now loaded

**Field-Editor** <br/>
#38 - first version of the editor, can load mubin/PrOD files
#38 - can save mubin files

**Misc**<br/>
#38 - sub-models now also search for textures with a base name (.1.bfres files are still not checked however)

<hr/>

### Version 1.0.0
First release!