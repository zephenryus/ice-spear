#  Ice-Spear - a Breath of the Wild Editor
![alt Ice-Spear](assets/icons/icon_256_thin.png)

For any information on how to use this, please see the Wiki: <br/>
https://gitlab.com/ice-spear-tools/ice-spear/wikis/home

<br />
### License
___
Licensed under GNU GPLv3.  
For more information see the LICENSE file in the project's root directory